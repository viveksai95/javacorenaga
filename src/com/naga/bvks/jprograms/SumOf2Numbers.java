package com.naga.bvks.jprograms;

import java.util.Scanner;

public class SumOf2Numbers {
	/**
	 * Starting point to Take input, perform Addition and print the result
	 * @param args
	 */
	public static void main(String[] args)
	{
		int digit1, digit2;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter Digit 1:");
		digit1 = input.nextInt();
		
		System.out.println("Enter Digit 2:");
		digit2 = input.nextInt();
		
		System.out.println("The Sum is :: " + performAddition(digit1, digit2));
		
		
		input.close();
	}
	
	/**
	 * Performs the core logic 4 addition
	 * @param digit1
	 * @param digit2
	 * @return Sum of digit1 & digit2
	 */
	private static int performAddition(int digit1, int digit2) {
		return digit1+digit2;
	}

}
